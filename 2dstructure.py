n = int(input())

BIT = [[0 for i in range(n + 1)] for j in range(n + 1)]
matrix = [[0 for i in range(n + 1)] for j in range(n + 1)]


def update(x, y, inc):
    row, col = x, y
    while (row <= n):
        col = y
        while (col <= n):
            BIT[row][col] += inc
            col += (col & (-col))
        row += (row & (-row))


def query(x, y):
    row, col = x, y
    sum = 0
    while (row > 0):
        col = y
        while (col > 0):
            sum += BIT[row][col]
            col -= (col & (-col))
        row -= (row & (-row))
    return sum

# Input matrix
for i in range(n):
    x = list(map(int, input().strip().split(' ')))
    for j in range(n):
        matrix[i][j] = x[j]
        update(i+1, j+1, matrix[i][j])

print("--")

for i in range(n):
    for j in range(n):
        print(matrix[i][j], end=" ")
    print()

print("--")

for i in range(1, n+1):
    for j in range(1, n+1):
        print(BIT[i][j], end=" ")
    print()

print("Queries:")

for _ in range(5):
    x, y = map(int, input().strip().split(' '))
    ans = query(x, y)
    print(ans)
