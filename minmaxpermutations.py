n, mod = map(int, input().strip().split(' '))

factorials = [0 for i in range(250005)]
factorials[0],factorials[1] = 1,1

for i in range(2, 250004):
    factorials[i] = (i * factorials[i - 1]) % mod

ans = 0
for k in range(1, n + 1):
    ans = (ans%mod + (((n - k + 1) % mod) * ((n - k + 1) % mod) * (factorials[k] % mod) * (factorials[n - k] % mod)) % mod)%mod

print(ans)

