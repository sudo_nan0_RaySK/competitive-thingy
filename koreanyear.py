n, m = map(int, input().strip().split(' '))

s = input().strip().split(' ')
t = input().strip().split(' ')

q = int(input())

for _ in range(q):
    year = int(input())
    sIndex, tIndex = year % n, year % m
    ans = s[sIndex - 1] + t[tIndex - 1]
    print(ans)