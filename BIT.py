n = int(input())

arr = list(map(int, input().strip().split(' ')))
BIT = [0 for _ in range(n+1)]


def update(index, increment):
    i = index
    while(i <= n):  # Log N
        BIT[i] += increment
        i += (i & (-i))  # parent


def query(index):
    sum = 0
    i = index
    while(i > 0):
        sum += BIT[i]
        i -= (i & (-i))
    return sum


# build BIT
for i in range(len(arr)):
    update(i + 1, arr[i])

print("BIT is :", BIT)

# queries
q = int(input())

for _ in range(q):
    x = int(input())  # sum till x
    print("Sum till {} is {}".format(x, query(x)))
