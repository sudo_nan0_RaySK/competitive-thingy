n =  int(input())

arr = list(map(int,input().strip().split(' ')))

BIT = [0 for _ in range(max(arr)+1)]

def update(index):
	i = index
	while(i<=n):
		BIT[i] += 1
		i += (i&(-i))

def query(index): 
	sum = 0 
	i = index
	while(i>0):
		sum += BIT[i]
		i -= (i&(-i))
	return sum

nInversions = 0

for i in range(n-1,-1,-1):
	element  = arr[i]
	#print("element is {}".format(element))
	nInversions += query(element-1)
	update(element)

print("total inversions are : {}".format(nInversions))
