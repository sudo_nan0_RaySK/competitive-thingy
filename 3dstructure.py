
BIT = [[[0 for i in range(105)] for j in range(105)] for k in range(105)]


def update(z, x, y, inc):
    l, b, h = x, y, z
    while (l <= 104):
        b = y
        while (b <= 104):
            h = z
            while (h <= 104):
                BIT[l][b][h] += inc
                h += (h & (-h))
            b += (b & (-b))
        l += (l & (-l))


def query(z, x, y):
    sum = 0
    l, b, h = x, y, z
    while (l > 0):
        b = y
        while (b > 0):
            h = z
            while (h > 0):
                sum += BIT[l][b][h]
                h -= (h & (-h))
            b -= (b & (-b))
        l -= (l & (-l))
    return sum


def query2d(x1, y1, x2, y2, z):
    return query(z, x2, y2)-query(z, x2, y1-1)-query(z, x1-1, y2)+query(z, x1-1, y1-1)


def query3d(x1, y1, z1, x2, y2, z2):
    return query2d(x1, y1, x2, y2, z2) - query2d(x1, y1, x2, y2, z1-1)


def operate(qString):
    op = qString.split(' ')
    if op[0] == 'UPDATE':
        x, y, z, inc = list(map(int, op[1:]))
        val1 = query(z,x,y)-query(z,x-1,y)-query(z,x,y-1)+query(z,x-1,y-1)
        val2 = query(z-1,x,y)-query(z-1,x-1,y)-query(z-1,x,y-1)+query(z-1,x-1,y-1)
        update(z, x, y, inc-(val1-val2))
    elif op[0] == 'QUERY':
        x1, y1, z1, x2, y2, z2 = list(map(int, op[1:]))
        ans = query3d(x1, y1, z1, x2, y2, z2)
        print(ans)
        
t = int(input())

for _ in range(t):
    BIT = [[[0 for i in range(105)] for j in range(105)] for k in range(105)]
    N, M = map(int, input().strip().split(' '))
    for _ in range(M):
        op = input()
        operate(op)
