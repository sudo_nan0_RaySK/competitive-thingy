
def getLastSetBitPos(n):
    mask = n & (-n)
    pos = 0
    while(mask != 0):
        if(mask & 1):
            return pos
        pos += 1
        mask = mask >> 1
    return pos

# Generating parents and ranges:


for i in range(1, 9):
    # parent:
    parent = i - (i & (-i)) 
    # range
    l = i - (2**getLastSetBitPos(i)) + 1
    r = i
    print("The parent of {} is {} and range stored is [{},{}]".format(
        i, parent, l, r))
