BIT = [0 for i in range((1000005))]

def update(index, increment):
    i = index
    while(i <= 1000002):  # Log N
        BIT[i] += increment
        i += (i & (-i))  # parent


def query(index):
    sum = 0
    i = index
    while(i > 0):
        sum += BIT[i]
        i -= (i & (-i))
    return sum

n = int(input())

bigArr = []

ans = 0

for _ in range(n):
    x = list(map(int, input().strip().split(' ')))
    bigArr.extend(x[1:])

for e in bigArr:
    update(e+1, 1)
    
for e in bigArr:
    ans += query(e)
    
print()
print(ans)
