diffDict = {}
for i in range(10):
    diffDict[i] = 0
for i in range(10):
    for j in range(i+1,10):
        print("Difference b/w ({},{}) is {}".format(i, j, (j - i)))
        diffDict[(j - i)] += 1

for key in diffDict.keys():
    print("No. of pairs with diff {} are {}".format(key,diffDict[key]))